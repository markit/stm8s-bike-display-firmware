#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "state.h"

struct FromController {
	// 0x41
	uint8_t header;
	uint8_t batteryLevel;
	// volts
	uint8_t voltage;
	// ms
	uint16_t rotation_period;
	uint8_t errorCode;
	uint8_t crc;
	uint8_t flags;
	// 1/4 amp per inc
	uint8_t current;
	// C-15
	int8_t temperature;
	uint8_t footer[2];
};

static const char* error_codes[6] = {
	"Throttle Abnormal",
	"",
	"Hall Sensor Abnormal",
	"Torque Sensor Abnormal",
	"Speed Sensor Abnormal",
	"Short Circuit"
};

struct ToController {
	// Power monitoring mode 0 = voltage, 4-11 lithium 24v, 5-15 lithium 36v
	uint8_t parameter5;
	// Lighting is msb
	// 6 == walk mode
	uint8_t assist_level;
	// Parameters 1&2 combined
	// Max speed - 10
	// wheelsize
	// p2 == pulses from wheel sensor per revolution
	// p3 == 1 if torque imitation, 0 if speed control (pas five mode)
	// p4 == 1 handlebar is dead until pedals move, 0 = handlebar only mode no pedal required
	uint8_t parameters1;
	// p1 == gear reduction * motor magnets
	uint8_t parameter1;
	uint8_t parameters2;
	uint8_t crc;
	// c1 pas sensor settings, sensitivity? 0-2 forward only 5-7 reverse capable lower == more sensitive
	// c2 motor phase classification? default = 0
	uint8_t parameterc1c2;
	// c5 current limiter (10 = default full, 3 == half)
	// c14 power assist strength multiplier 1,2,3 3 strongest
	uint8_t parameterc5c14;
	// c4 Handlebar startup mode additions (use 0)
	uint8_t parameterc4;
	// c12 minimum voltage thresholds (default = 4)
	uint8_t parameterc12;
	// c13 energy recovery 0 = off 1 = max 5 = worse, but stronger braking?
	uint8_t parameterc13;
	uint8_t fifty;
	uint8_t fourteen;
};

void send_message(struct ToController const*const message);
void set_crc(struct ToController *const message);
void init_message(struct ToController *const message);
bool check_crc(struct FromController const*const message);
void push_state(struct ToController *const message, struct State const*const state);
