#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "ht1621.h"
#include "state.h"

#define LCD_TYPE 6

#define TYPE_A_SEG_A 0x08
#define TYPE_A_SEG_B 0x04
#define TYPE_A_SEG_C 0x02
#define TYPE_A_SEG_D 0x01
#define TYPE_A_SEG_E 0x10
#define TYPE_A_SEG_F 0x40
#define TYPE_A_SEG_G 0x20

#define TYPE_B_SEG_A 0x80
#define TYPE_B_SEG_B 0x40
#define TYPE_B_SEG_C 0x20
#define TYPE_B_SEG_D 0x10
#define TYPE_B_SEG_E 0x01
#define TYPE_B_SEG_F 0x04
#define TYPE_B_SEG_G 0x02

#define TYPE_C_SEG_A 0x80
#define TYPE_C_SEG_B 0x40
#define TYPE_C_SEG_C 0x20
#define TYPE_C_SEG_D 0x10
#define TYPE_C_SEG_E 0x02
#define TYPE_C_SEG_F 0x08
#define TYPE_C_SEG_G 0x04

#define TYPE_D_SEG_A 0x08
#define TYPE_D_SEG_B 0x04
#define TYPE_D_SEG_C 0x02
#define TYPE_D_SEG_D 0x01
#define TYPE_D_SEG_E 0x20
#define TYPE_D_SEG_F 0x80
#define TYPE_D_SEG_G 0x40

#define CC2(a,b) a##b
#define CC(a,b) CC2(a,_SEG_##b)
#define DIGIT_0(type) (CC(type, A) | CC(type, B) | CC(type, C) | CC(type, D) | CC(type, E) | CC(type, F))
#define DIGIT_1(type) (CC(type, B) | CC(type, C))
#define DIGIT_2(type) (CC(type, A) | CC(type, B) | CC(type, D) | CC(type, E) | CC(type, G))
#define DIGIT_3(type) (CC(type, A) | CC(type, B) | CC(type, C) | CC(type, D) | CC(type, G))
#define DIGIT_4(type) (CC(type, B) | CC(type, C) | CC(type, F) | CC(type, G))
#define DIGIT_5(type) (CC(type, A) | CC(type, C) | CC(type, D) | CC(type, F) | CC(type, G))
#define DIGIT_6(type) (CC(type, A) | CC(type, C) | CC(type, D) | CC(type, E) | CC(type, F) | CC(type, G))
#define DIGIT_7(type) (CC(type, A) | CC(type, B) | CC(type, C))
#define DIGIT_8(type) (CC(type, A) | CC(type, B) | CC(type, C) | CC(type, D) | CC(type, E) | CC(type, F) | CC(type, G))
#define DIGIT_9(type) (CC(type, A) | CC(type, B) | CC(type, C) | CC(type, D) | CC(type, F) | CC(type, G))

#if LCD_TYPE==6
#define FRAMEBUFFER_SIZE 24
#endif

struct lcd {
	uint8_t framebuffer[FRAMEBUFFER_SIZE];
	// Whether to use display units as km units or convert
	bool km;
	bool celsius;
	uint8_t frame;
	struct HT1621 display;
#if LCD_TYPE==6
	struct HT1621 display_secondary;
#endif
	uint8_t *port_backlight;
	uint8_t pin_backlight;
};

void lcd_init(struct lcd *const l);
void lcd_clear_framebuffer(struct lcd *const l);
void lcd_draw(struct lcd *const l);

void lcd_set_backlight(struct lcd *const l, const bool enable);
void lcd_set_brake(struct lcd *const l, const bool applied);

void lcd_set_time(struct lcd *const l, const uint16_t minutes, const uint8_t seconds);

/*
 * @param level Between 0 and 4
 */
void lcd_set_battery(struct lcd *const l, const uint8_t level);

void lcd_set_speed_area(struct lcd *const l, const enum ControllerMode mode);
/*
 * @param dkm Speed in tenths of a km
 */
void lcd_set_speed(struct lcd *const l, const uint16_t dkm, const bool average, const bool max);

void lcd_set_pas(struct lcd *const l, const uint8_t pas);
void lcd_set_cadence(struct lcd *const l, const uint8_t cadence);

void lcd_set_motor_area(struct lcd *const l, const uint16_t value, const bool motor, const bool temp, const bool watt);

void lcd_set_env_temp(struct lcd *const l, const int value);

void lcd_set_odo_area(struct lcd *const l, const uint16_t value, const bool trip, const bool voltage);

void lcd_set_status(struct lcd *const l, const struct State *const state);
