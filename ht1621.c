#include "ht1621.h"
#include "gpio.h"
#include <delay.h>

void ht1621_init(const struct HT1621 *const ht) {
//	gpio_set_direction_output(ht->port_w, ht->pin_w);
//	gpio_set_direction_output(ht->port_cs, ht->pin_cs);
//	gpio_set_direction_output(ht->port_data, ht->pin_data);

	gpio_write(ht->port_cs, ht->pin_cs, true);
	gpio_write(ht->port_w, ht->pin_w, true);
	gpio_write(ht->port_data, ht->pin_data, true);
}

void ht1621_send_commands(const struct HT1621 *const ht, const uint8_t *const commands, const uint8_t count) {

	// Send commands
	for(uint8_t i = 0; i < count; ++i) {
		gpio_write(ht->port_cs, ht->pin_cs, false);
		// Set to command mode send extra 0 as commands include extra 0 for don't care bit
		ht1621_write_bits(ht, COMMAND_MODE, 4);
		ht1621_write_bits(ht, commands[i], 8);
		gpio_write(ht->port_cs, ht->pin_cs, true);
	}

}

void ht1621_write_bits(const struct HT1621 *const ht, uint8_t bits, const uint8_t count) {
	for(uint8_t i = 0; i < count; ++i, bits <<= 1) {
		gpio_write(ht->port_w, ht->pin_w, false);
		gpio_write(ht->port_data, ht->pin_data, (bits & 0x80) ? true : false);
		gpio_write(ht->port_w, ht->pin_w, true);
	}
}

void ht1621_write_byte(const struct HT1621 *const ht, const uint8_t address, uint8_t data) {
	gpio_write(ht->port_cs, ht->pin_cs, false);

	// Set to write mode
	ht1621_write_bits(ht, WRITE_MODE, 3);
	// 128 addresses
	ht1621_write_bits(ht, address<<3, 6);
	// Data
	for(uint8_t i = 0; i < 2; ++i, data <<= 4) {
		ht1621_write_bits(ht, data, 4);
	}

	gpio_write(ht->port_cs, ht->pin_cs, true);
}

void ht1621_write_nibble(const struct HT1621 *const ht, const uint8_t address, uint8_t data) {
	gpio_write(ht->port_cs, ht->pin_cs, false);

	// Set to write mode
	ht1621_write_bits(ht, WRITE_MODE, 3);
	// 128 addresses
	ht1621_write_bits(ht, address<<2, 6);
	// Data
	ht1621_write_bits(ht, data, 4);

	gpio_write(ht->port_cs, ht->pin_cs, true);
}

void ht1621_write_buffer(const struct HT1621 *const ht, const uint8_t address, const uint8_t *const data, const uint8_t nibbles) {
	gpio_write(ht->port_cs, ht->pin_cs, false);

	// Set to write mode
	ht1621_write_bits(ht, WRITE_MODE, 3);
	// 128 addresses
	ht1621_write_bits(ht, address<<2, 6);
	// Data
	uint8_t d;
	for(uint8_t j = 0; j < nibbles; ++j) {
		if (j%2 == 0) {
			d = data[j/2];
		} else {
			d <<= 4;
		}
		ht1621_write_bits(ht, d, 4);
	}

	gpio_write(ht->port_cs, ht->pin_cs, true);
}
