#pragma once

#include <stdint.h>

/*
 * Modes and Command list adapted from code written by Enrico Formenti
 */

static const uint8_t COMMAND_MODE = 0b10000000; /*!< This is used for sending standard commands. */
static const uint8_t READ_MODE = 0b11000000; /*!< This instructs the HT1621 to prepare for reading the internal RAM. */
static const uint8_t WRITE_MODE = 0b10100000; /*!< This instructs the HT1621 to prepare for writing the internal RAM. */
static const uint8_t READ_MODIFY_WRITE_MODE = 0b10100000; /*!< This instructs the HT1621 to prepare for reading/modifying batch of internal RAM adresses. */
static const uint8_t SPECIAL_MODE = 0b10010000; /*!< This instructs the HT1621 to prepare for executing a special command. */

static const uint8_t SYS_DIS   = 0b00000000; /*!< System disable. It stops the bias generator and the system oscillator. */
static const uint8_t SYS_EN    = 0b00000010; /*!< System enable. It starts the bias generator and the system oscillator. */
static const uint8_t LCD_OFF   = 0b00000100; /*!< Turn off the bias generator. */
static const uint8_t LCD_ON    = 0b00000110; /*!< Turn on the bias generator. */
static const uint8_t TIMER_DIS = 0b00001000; /*!< Disable time base output. */
static const uint8_t WDT_DIS   = 0b00001010; /*!< Watch-dog timer disable. */
static const uint8_t TIMER_EN  = 0b00001100; /*!< Enable time base output. */
static const uint8_t WDT_EN    = 0b00001110; /*!< Watch-dog timer enable. The timer is reset. */
static const uint8_t CLR_TIMER = 0b00011000; /*!< Clear the contents of the time base generator. */
static const uint8_t CLR_WDT   = 0b00011100; /*!< Clear the contents of the watch-dog stage. */

static const uint8_t TONE_OFF  = 0b00010000; /*!< Stop emitting the tone signal at the tone pin. \sa TONE2K, TONE4K */
static const uint8_t TONE_ON   = 0b00010010; /*!< Start emitting tone signal at the tone pin. Tone frequency is selected using commands TONE2K or TONE4K. \sa TONE2K, TONE4K */
static const uint8_t TONE2K    = 0b11000000; /*!< Output tone is at 2kHz. */
static const uint8_t TONE4K    = 0b10000000; /*!< Output tone is at 4kHz. */

static const uint8_t RC256K    = 0b00110000; /*!< System oscillator is the internal RC oscillator at 256kHz. */
static const uint8_t XTAL32K   = 0b00101000; /*!< System oscillator is the crystal oscillator at 32768Hz. */
static const uint8_t EXT256K   = 0b00111000; /*!< System oscillator is an external oscillator at 256kHz. */

//Set bias to 1/2 or 1/3 cycle
//Set to 2;3 or 4 connected COM lines
static const uint8_t BIAS_HALF_2_COM  = 0b01000000; /*!< Use 1/2 bias and 2 commons. */
static const uint8_t BIAS_HALF_3_COM  = 0b01001000; /*!< Use 1/2 bias and 3 commons. */
static const uint8_t BIAS_HALF_4_COM  = 0b01010000; /*!< Use 1/2 bias and 4 commons. */
static const uint8_t BIAS_THIRD_2_COM = 0b01000010; /*!< Use 1/3 bias and 2 commons. */
static const uint8_t BIAS_THIRD_3_COM = 0b01001010; /*!< Use 1/3 bias and 3 commons. */
static const uint8_t BIAS_THIRD_4_COM = 0b01010010; /*!< Use 1/3 bias and 4 commons. */

static const uint8_t IRQ_EN    = 0b00010000; /*!< Enables IRQ output. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t IRQ_DIS   = 0b00010000; /*!< Disables IRQ output. This needs to be excuted in SPECIAL_MODE. */

// WDT configuration commands
static const uint8_t F1 = 0b01000000; /*!< Time base/WDT clock. Output = 1Hz. Time-out = 4s. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t F2 = 0b01000010; /*!< Time base/WDT clock. Output = 2Hz. Time-out = 2s. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t F4 = 0b01000100; /*!< Time base/WDT clock. Output = 4Hz. Time-out = 1s. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t F8 = 0b01000110; /*!< Time base/WDT clock. Output = 8Hz. Time-out = .5s. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t F16 = 0b01001000; /*!< Time base/WDT clock. Output = 16Hz. Time-out = .25s. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t F32 = 0b01001010; /*!< Time base/WDT clock. Output = 32Hz. Time-out = .125s. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t F64 = 0b01001100; /*!< Time base/WDT clock. Output = 64Hz. Time-out = .0625s. This needs to be excuted in SPECIAL_MODE. */
static const uint8_t F128 = 0b01001110; /*!< Time base/WDT clock. Output = 128Hz. Time-out = .03125s. This needs to be excuted in SPECIAL_MODE. */

// Don't use?
static const uint8_t TEST_ON   = 0b11000000; /*!< Don't use! Only for manifacturers. This needs SPECIAL_MODE. */
static const uint8_t TEST_OFF  = 0b11000110;  /*!< Don't use! Only for manifacturers. This needs SPECIAL_MODE. */

struct HT1621 {
	uint8_t *port_w;
	uint8_t *port_cs;
	uint8_t *port_data;
	uint8_t pin_w;
	uint8_t pin_cs;
	uint8_t pin_data;
};

void ht1621_init(const struct HT1621 *const ht);
void ht1621_send_commands(const struct HT1621 *const ht, const uint8_t *const commands, const uint8_t count);
void ht1621_write_bits(const struct HT1621 *const ht, uint8_t bits, const uint8_t count);
/**
 * @param address Refers to a byte
 */
void ht1621_write_byte(const struct HT1621 *const ht, const uint8_t address, uint8_t data);

void ht1621_write_nibble(const struct HT1621 *const ht, const uint8_t address, uint8_t data);
/**
 * @param address Refers to a nibble
 */
void ht1621_write_buffer(const struct HT1621 *const ht, const uint8_t address, const uint8_t *const data, const uint8_t nibbles);
