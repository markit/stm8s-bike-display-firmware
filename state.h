#pragma once

#include <stdint.h>
#include <stdbool.h>

enum ControllerMode {
	Cruise,
	PAS,
	Throttle,
	Assist,
	Unknown,
};

struct State {
	/**
	 * Battery State
	 */
	uint8_t battery_state;

	/**
	 * Voltage in tenths of a volt
	 */
	uint16_t voltage;

	/**
	 * Voltage in tenths of a volt
	 */
	uint16_t local_voltage;

	/**
	 * Speed in tenths of a km/h
	 */
	uint16_t speed;

	/**
	 * Strength of assist, between 0-9
	 */
	uint8_t pas_value;

	/**
	 * Time in seconds
	 */
	uint32_t uptime;

	/**
	 *
	 */
	uint8_t cadence;

	/**
	 * 4xAmps
	 */
	uint8_t current;

	enum ControllerMode mode;

	bool braking;
	bool lights;

	int8_t env_temp;

	int8_t controller_temp;

	uint16_t trip_odometer;

	uint16_t total_odometer;

	float wheel_circumference;
};
