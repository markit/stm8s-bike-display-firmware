#include "original.h"
#include <uart.h>

void set_crc(struct ToController *const message) {
	message->crc = 0;
	uint8_t crc = 0;
	uint8_t *const ptr = (uint8_t *const) message;

	for(uint8_t i = 0; i < sizeof(struct ToController); ++i) {
		crc ^= ptr[i];
	}

	// Can be another number
	crc ^= 2;

	message->crc = crc;
}

bool check_crc(struct FromController const*const message) {
	uint8_t crc = 0;
	uint8_t *const ptr = (uint8_t *const) message;

	for(uint8_t i = 1; i < sizeof(struct FromController); ++i) {
		if (i != 6) {
			crc ^= ptr[i];
		}
	}

	return crc == message->crc;
}

void init_message(struct ToController *const message) {
	message->fifty = 50;
	message->fourteen = 14;
}

void send_message(struct ToController const*const message) {
	uint8_t *const ptr = (uint8_t *const) message;

	for(uint8_t i = 0; i < sizeof(struct ToController); ++i) {
		uart_write(ptr[i]);
	}
}

void push_state(struct ToController *const message, struct State const*const state) {
	// Pas + lights
	message->assist_level = state->pas_value/2 | (state->lights << 7);

	// TODO these are hard coded parameters
	// Power monitoring method
	message->parameter5 = 0;
	// Regen off atm
	message->parameterc13 = 0;
	// Default min voltage threshold
	message->parameterc12 = 4;
	message->parameterc4 = 0;

	// Current limiter
	const uint8_t c5 = 10;
	// Power assist strength
	const uint8_t c14 = 3;
	message->parameterc5c14 = c5 | (c14 << 5);

	// PAS sensor setting
	const uint8_t c1 = 0;
	// Motor phase classification
	const uint8_t c2 = 0;
	message->parameterc1c2 = c2 | (c1 << 3);
	
	// Number of magnets
	message->parameter1 = 12;

	const uint8_t wheel_size = 0x14;
	const uint8_t max_speed = 15; // 15+10 = 25km/h
	const uint8_t torque_imitation = 1;
	const uint8_t handlebar_safe_mode = 0;
	const uint8_t wheel_sensor_ppr = 1;
	message->parameters1 = (max_speed << 3) | (wheel_size >> 2);
	message->parameters2 = (max_speed & (1<<5)) | (wheel_size << 6) | wheel_sensor_ppr | (torque_imitation << 3) | (handlebar_safe_mode << 4);
}
