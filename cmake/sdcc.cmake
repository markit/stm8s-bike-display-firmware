find_program(STM8FLASH stm8flash
	DOC "stm8flash binary location"
)

set(STM8FLASH_PROGRAMMER
	"stlinkv2" CACHE STRING
	"Choice of stlink, stlinkv2 or espstlink, when not specified in target properties")

function(add_upload_to_target target)
	get_target_property(FAMILY ${target} FAMILY)

	if (FAMILY STREQUAL "stm8")
		if (STM8FLASH STREQUAL "STM8FLASH-NOTFOUND")
			message(SEND_ERROR "stm8flash is not found")
		endif()

		get_target_property(STM8_PART ${target} PART)
		get_target_property(PROGRAMMER ${target} PROGRAMMER)
		if(PROGRAMMER STREQUAL "PROGRAMMER-NOTFOUND")
			set(PROGRAMMER ${STM8FLASH_PROGRAMMER})
		endif()

		add_custom_target(${target}-upload
			COMMAND ${STM8FLASH} -c ${PROGRAMMER} -p ${STM8_PART} -w ${target}.ihx
			DEPENDS ${target}
			COMMENT "Uploading ${target}"
		)
	endif()
endfunction(add_upload_to_target)

function(add_definitions_to_target target)
		get_target_property(STM8_PART ${target} PART)
		get_target_property(STM8_FREQ ${target} FREQ)
		string(SUBSTRING ${STM8_PART} 5 3 STM8_FAMILY)

		target_compile_definitions(${target}
			PUBLIC
				F_CPU=${STM8_FREQ}UL
				CPU_PART=${STM8_PART}
				CPU_FAMILY=${STM8_FAMILY}
		)

endfunction(add_definitions_to_target)
