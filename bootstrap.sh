if [ ! -f stm8-bare-min/README.md ]; then
	git submodule init
	git submodule update
fi

mkdir build
cd build
cmake ../ -DCMAKE_TOOLCHAIN_FILE=../cmake/sdcc-toolchain.cmake
