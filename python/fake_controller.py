#!/usr/bin/env python3


import serial
import argparse


class FromController:
    def __init__(self, array=None):
        if array != None:
            self.array = [0x41]
            self.array.extend(array)
        else:
            self.array = [0]*12
            self.array[-1] = 0
            self.array[-2] = 0
            self.array[0] = 0x41

    @staticmethod
    def read(ser):
        # Find end of last message
        while ser.read() != b'\x41':
            print('Skipping byte')
            pass

        # Read 11 bytes, one less because we have the one from above
        msg = FromController(ser.read(11))

        return msg

    def is_valid(self):
        crc = 0
        for i, b in enumerate(self.array):
            if i != 6:
                crc ^= b
        return crc == self.array[6]


    def set_crc(self):
        crc = 0
        for i, b in enumerate(self.array):
            if i != 6:
                crc ^= b
        self.array[6] = crc


    def set_battery_level(self, battery_level):
        self.array[1] = battery_level


    def set_voltage(self, voltage):
        self.array[2] = voltage


    def get_voltage(self):
        return self.array[2]


    def set_current(self, current):
        self.array[-4] = int(current*4)


    def send(self, ser):
        ser.write(bytes(self.array))


class ToController:
    def __init__(self, array=None):
        if array == None:
            self.array = [0]*13
        else:
            # Attach last bit of footer
            array+= b'\x0e'

            self.array = array


    def is_valid(self):
        crc = 0
        for i, b in enumerate(self.array):
            if i != 5:
                crc ^= b
        crc ^= 2
        return crc == self.array[5]


    def get_assist_level(self):
        return self.array[1] & 0x07


    def is_light_on(self):
        return self.array[1] & 0x80 != 0


    def send(self, ser):
        ser.write(self.array)


    def __str__(self):
        return '<ToController pas={}, lights={}>'.format(self.get_assist_level(), self.is_light_on())


    def __repr__(self):
        return 'ToController({})'.format(self.array)


    def send(self, ser):
        ser.write(bytes(self.array))


    def set_crc(self):
        crc = 0
        for i, b in enumerate(self.array):
            if i != 5:
                crc ^= b
        self.array[5] = crc ^ 2


    @staticmethod
    def read(ser):
        # Find end of last message
        while ser.read() != b'\x0e':
            print('Skipping byte')
            pass

        # Read 12 bytes, one less because we have the one from above
        msg = ToController(ser.read(12))

        return msg

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fake a motor controller')
    parser.add_argument('--port', default='/dev/ttyUSB0')
    parser.add_argument('--baudrate', default=9600, type=int)
    parser.add_argument('--mode', default='controller')

    args = parser.parse_args()

    ser = serial.Serial(args.port, args.baudrate)

    if args.mode == 'controller':
        while True:
            msg = ToController.read(ser)
            if msg.is_valid():
                print(msg)
                print(msg.array)
                
                out = FromController()
                out.set_battery_level(5)
                out.set_voltage(10)
                out.set_current(4.25)
                out.set_crc()
                out.send(ser)
            else:
                print("Message no good")
                print(msg.array)
    else:
        while True:
            msg = FromController.read(ser)
            print(msg.is_valid())
            print(msg.get_voltage())
            print(msg.array)
            #msg = ToController()
            #msg.set_crc()
            #msg.send(ser)

            #print(ser.in_waiting)

