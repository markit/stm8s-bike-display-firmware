```
L     CCC DDD    6
L    C    D  D  6
L    C    D  D 6666
L    C    D  D 6   6
LLLL  CCC DDD   666
```
# LCD6 Custom Firmware

## Requirements
* You will need a more recent version of SDCC than that which is provided by
Debian Buster.

* `stm8flash`

## Setup
Run `./bootstrap.sh` to run cmake with the required commands to setup the
`build` directory.

## Flashing
You will need a stlink v2 or SWIM alternative to flash the device.
You'll need to open the device and find the 4 pin connector on the back of the pcb, the pinout is as follows:
 * pin 1 5v (This is the one boxed in white)
 * pin 2 SWIM
 * pin 3 GND
 * pin 4 RST

If the device is powered from a battery, you will need to hold the power button down for the flashing process to work. You do not need to connect the 5V if this is the case. If the 5V is connected then you do not need to hold the power down to flash the device.

Unplugging the device from the controller first is recommended. The stm8 has copy protection enabled, you need to disable this by writing the following option code (Instructions for linux).
`echo "00 00 ff 00 ff 00 ff 00 ff 00 ff 00 ff 00 ff" | xxd -r -p > erase_original_firmware_and_unlock.bin`
`stm8flash -c stlinkv2 -p stm8s105?6 -s opt -w erase_original_firmware_and_unlock.bin`
This is basically like reseting the chip to factory settings, but means you cannot go back to the original firmware.

Once this is complete, you can flash new firmware with `make display-firmware-upload`.

## Internals
Internally, the LCD6 is very similar to the LCD3, with the same processor, but
it has 2 display controllers to cope with the additional LCD segments. The
power segment of both devices appears to be identical, but the remaining chip
pins have been rearranged.

The CPU is clocked at 16Mhz, with an internal clock. The on display clock isn't
completely accurate, and could drift with temperature.

### Pinout
The CPU pins currently understood are listed below:
* *PA2* Power latch, on battery this needs to be high to keep the device
  powered after releasing the power button.
* *PB5 & 6* Up and down buttons, active low, with debounce circuit.
* *PG1* Power button.
* *PE6* Pin on the 2 port PCB header.
* *PC4* LCD Backlight Enable.
* *ADC8* Voltage (Appears to be x1.5).
* *PD0* LCD1 CS
* *PA4* LCD1 Write Enable
* *PB7* LCD1 Data
* *PB0* LCD2 CS
* *PB1* LCD2 Write Enable
* *PB2* LCD2 Data
* *PD5* TX
* *PD6* RX
* *Missing* Temperature sensor
