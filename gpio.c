#include "gpio.h"

volatile void gpio_set_direction_input(volatile uint8_t *const port, const uint8_t pin) {
	*port &= ~(1<<pin);
}

volatile void gpio_set_direction_output(volatile uint8_t *const port, const uint8_t pin) {
	*port |= (1<<pin);
}

