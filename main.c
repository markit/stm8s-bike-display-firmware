#include <stm8s.h>
#include <stdint.h>
#include <delay.h>
#include <uart.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "gpio.h"
#include "ht1621.h"
#include "lcd.h"
#include "original.h"

#define LED_PIN     6
#define ENABLE_POWER_LCD_BACKLIGHT 2
#define ENABLE_LCD_BACKLIGHT 4

static volatile struct State state;
volatile uint16_t counter = 0;
volatile bool update_display = true;
volatile bool update_input = true;
volatile bool perform_i = false;
volatile bool perform_o = true;
# define MAX_PULSES_SINCE_VALID_INPUT 200
volatile uint16_t pulses_since_valid_input = 200;

// Pulse every 2.048ms
void timer_isr() __interrupt(TIM4_ISR) {
	if (counter % 50 == 0) {
		update_display = true;
		perform_o = true;
	}

	if (counter % 10 == 0) {
		update_input = true;
	}

	if (counter++ == 488) {
		++state.uptime;
		counter = 0;
	}

	if (pulses_since_valid_input < MAX_PULSES_SINCE_VALID_INPUT) {
		++pulses_since_valid_input;
	}

	gpio_write(&PE_ODR, 6, (counter % 2) == 0);

    TIM4_SR &= ~(1 << TIM4_SR_UIF);
}

uint8_t rolling_buffer[sizeof(struct FromController)];
uint8_t rolling_next = 0;
volatile struct FromController in_message;

inline bool check_buffer() {
	const uint8_t position[] = {0, 10, 11};
	const uint8_t value[] = {0x41, 0, 0};
	bool valid = true;

	for(uint8_t i = 0; valid && i < 3; ++i) {
		valid = rolling_buffer[rolling_next+position[i] % sizeof(struct FromController)] == value[i];
	}

	return valid;
}

void uart_rx() __interrupt(UART2_RXC_ISR) {
	rolling_buffer[rolling_next] = uart_read();
	rolling_next = (rolling_next + 1) % sizeof(struct FromController);
	if (check_buffer()) {
		perform_i = true;
		uint8_t *ptr = (uint8_t *) &in_message;
		for(uint8_t i = 0; i < sizeof(struct FromController); ++i) {
			ptr[i] = rolling_buffer[i];
		}
	} else {
		perform_i = false;
	}
}

//
//  Setup the system clock to run at 16MHz using the internal oscillator.
//
void internal_clock_full_speed() {
	CLK_ICKR = 0;                       //  Reset the Internal Clock Register.
	CLK_ICKR = 1<<CLK_ICKR_HSIEN;                 //  Enable the HSI.
	CLK_ECKR = 0;                       //  Disable the external clock.
	while (!(CLK_ICKR & (1<<CLK_ICKR_HSIRDY)));       //  Wait for the HSI to be ready for use.
	CLK_CKDIVR = 0;                     //  Ensure the clocks are running at full speed.
	CLK_PCKENR1 = 0xff;                 //  Enable all peripheral clocks.
	CLK_PCKENR2 = 0xff;                 //  Ditto.
	CLK_CCOR = 0;                       //  Turn off CCO.
	CLK_HSITRIMR = 0;                   //  Turn off any HSIU trimming.
	CLK_SWIMCCR = 0;                    //  Set SWIM to run at clock / 2.
	CLK_SWR = 0xe1;                     //  Use HSI as the clock source.
	CLK_SWCR = 0;                       //  Reset the clock switch control register.
	CLK_SWCR = 1<<CLK_SWCR_SWEN;                  //  Enable switching.
	while (CLK_SWCR & (1<<CLK_SWCR_SWBSY));        //  Pause while the clock switch is busy.
}

static void iwdg_enable() {
    // enable IWDG
    IWDG_KR = IWDG_KEY_ENABLE;

    // enable write access
    IWDG_KR = IWDG_KEY_ACCESS;

    // prescaler = 64
    IWDG_PR |= 4;

    // timeout = 250ms
    IWDG_RLR = 0xFA;

    // reload watchdog counter
    IWDG_KR = IWDG_KEY_REFRESH;
}

inline void iwdg_refresh() {
    IWDG_KR = IWDG_KEY_REFRESH;
}

/*
 * Redirect stdout to UART
 */
int putchar(int c) {
    uart_write(c);
    return 0;
}

/*
 * Redirect stdin to UART
 */
int getchar() {
    return uart_read();
}

uint16_t ADC_read(uint8_t channel) {
    // configure ADC channel 4 (PD3)
    ADC1_CSR |= 0x0f & channel;

    // right-align data
    ADC1_CR2 |= (1 << ADC1_CR2_ALIGN);

    // wake ADC from power down
    ADC1_CR1 |= (1 << ADC1_CR1_ADON);

	// Start conversion
    ADC1_CR1 |= (1 << ADC1_CR1_ADON);
	// Wait
    while (!(ADC1_CSR & (1 << ADC1_CSR_EOC)));
	uint16_t val = (unsigned int)ADC1_DRL;
    val |= (unsigned int)ADC1_DRH<<8;
    ADC1_CSR &= ~(1 << ADC1_CSR_EOC); // clear EOC flag
    return val & 0x03ff;
}

void main() {
	disable_interrupts();
	internal_clock_full_speed();
	// UART does not init without this delay
	delay_ms(1);
	uart_init(9600, true);
	iwdg_enable();
	enable_interrupts();
	delay_ms(1);

	// UART
	PD_DDR = 1<<5;

    /* Prescaler = 128 (maximum)*/
	TIM4_PSCR = 0b00000111;

    /* Frequency = F_CLK / (2 * prescaler * (1 + ARR))
     *           = 2 MHz / (2 * 128 * (1 + 77)) = 100 Hz */
    TIM4_ARR = 65535;

    TIM4_IER |= (1 << TIM4_IER_UIE); // Enable Update Interrupt
    TIM4_CR1 |= (1 << TIM4_CR1_CEN); // Enable TIM4


    PE_DDR |= (1 << LED_PIN);
    PE_CR1 |= (1 << LED_PIN);

	PA_DDR = 0x00;
	PA_CR1 = 0xff;
	PB_DDR = 0xff;

	// Power Latch
	gpio_set_direction_output(&PA_DDR, 2);
	// Latch keeps the power on
	gpio_write(&PA_ODR, 2, true);
	
	// Up/Down Buttons
	gpio_set_direction_input(&PB_DDR, 5);
	gpio_set_direction_input(&PB_DDR, 6);

	// Power Button
	gpio_set_direction_input(&PG_DDR, 1);

	// Testing
	// Testing end

	PB_CR1 = 0xff;
	PC_DDR = 0xff;
	PC_CR1 = 0xff;
	//PD_CR1 = 0xff;

	PE_DDR = 0xff;
	PE_CR1 = 0xff;

	// Header pin
	gpio_set_direction_output(&PE_DDR, 6);

	// Backlight
	gpio_set_direction_output(&PC_DDR, 4);

	struct lcd display;
	display.km = true;
	display.celsius = true;
	display.port_backlight = &PC_ODR;
	display.pin_backlight = 4;
	lcd_init(&display);
	
	state.lights = true;
	state.wheel_circumference = 2.12;

	// In fiftyths of a second
	uint16_t press_time = 0;
	bool both_pressed = false;

	//int j = 0;
	while(true) {
		if (update_input) {
			update_input = false;

			if (gpio_read(&PG_IDR, 1) || !gpio_read(&PB_IDR, 6) || !gpio_read(&PB_IDR, 5)) {

				if (!gpio_read(&PB_IDR, 6) && !gpio_read(&PB_IDR, 5)) {
					both_pressed = true;
				}

				// Max 20sec press time
				if (press_time < 1000) {
					press_time += 1;
				}

			} else if (press_time > 0) {
				press_time = 0;
				both_pressed = false;
			}

			if (gpio_read(&PG_IDR, 1)) {
				if (press_time > 200) {
					press_time = 0;

					// Latch off
					gpio_write(&PA_ODR, 2, false);
				}
			}

			if (!gpio_read(&PB_IDR, 6) && !gpio_read(&PB_IDR, 5)) {
				if (press_time > 50) {
					press_time = 0;
					both_pressed = false;
					state.lights = !state.lights;
				}
			} else if (!both_pressed && !gpio_read(&PB_IDR, 6) && press_time > 10) {
				press_time = 0;
				if (state.pas_value > 0) {
					state.pas_value-= 1;
				}
			} else if (!both_pressed && !gpio_read(&PB_IDR, 5) && press_time > 10) {
				press_time = 0;
				if (state.pas_value < 9) {
					state.pas_value+= 1;
				}
			}
		}

		if (update_display) {
			update_display = false;
			//state.env_temp = ADC_read(4);
			lcd_clear_framebuffer(&display);
			lcd_set_status(&display, &state);
			lcd_draw(&display);
			/*
			lcd_set_time(&display, 678, (j%(FRAMEBUFFER_SIZE-16))+16);
			lcd_set_backlight(&display, true);
			lcd_set_brake(&display, true);
			lcd_set_battery(&display, (j/4)%10);
			lcd_set_speed_area(&display, PAS);
			lcd_set_speed(&display, j, false, false);
			lcd_set_pas(&display, j%10);
			lcd_set_cadence(&display, j);
			lcd_set_motor_area(&display, j*100 + j, true, false, true);
			lcd_set_env_temp(&display, j-10);
			lcd_set_odo_area(&display, voltage, false, true);
			*/
			//++j;

			// Update watchdog
			iwdg_refresh();
		}
		
		if (perform_o) {
			perform_o = false;
			struct ToController message;
			init_message(&message);
			push_state(&message, &state);
			set_crc(&message);
			send_message(&message);
		}

		if (perform_i) {
			const bool crc_ok = check_crc(&in_message);

			if (!crc_ok) {
				state.env_temp = 11;
			} else {
				pulses_since_valid_input = 0;

				state.voltage = in_message.voltage * 10;
				state.current = in_message.current;
				state.controller_temp = in_message.temperature - 15;
				state.battery_state = in_message.batteryLevel;

				// circumference * 1000 / ms == metres/s
				const float mSec = state.wheel_circumference * 1000 / in_message.rotation_period;
				// *3.6 = km/h
				state.speed = mSec * 3.6;

				// TODO distance
				// TODO braking
				// TODO cadence

				switch (in_message.errorCode) {
					case 0x20:
					case 0x25:
					case 0x28:
						state.env_temp = 0;
						break;
					case 0x21:
						state.env_temp = 6;
						break;
					case 0x22:
						state.env_temp = 1;
						break;
					case 0x23:
						state.env_temp = 2;
						break;
					case 0x24:
						state.env_temp = 3;
						break;
					case 0x26:
						state.env_temp = 4;
						break;
					default:
						state.env_temp = 10;
						break;
				}

				if ((in_message.flags & 0x01) != 0) {
					state.mode = Throttle;
				} else if ((in_message.flags & 0x08) != 0) {
					state.mode = Cruise;
				} else if ((in_message.flags & 0x10) != 0) {
					state.mode = Assist;
				} else {
					state.mode = PAS;
				}
			}

			perform_i = false;
		}

		// Modifier was 1.7, 1.5 seems more accurate
		state.local_voltage = (float) ADC_read(8) /1.5f;

		if (pulses_since_valid_input == MAX_PULSES_SINCE_VALID_INPUT) {
			state.env_temp = 99;

			state.voltage = state.local_voltage;
			state.current = 0;
			state.controller_temp = 0;
			state.battery_state = 0;
		}

		wait_for_interrupts();
	}
}
