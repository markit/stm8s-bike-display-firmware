#include "lcd.h"
#include <string.h>
#include "gpio.h"
#include <stm8s.h>
#include <stdlib.h>

const uint8_t digit_type_a[] = {
	DIGIT_0(TYPE_A),
	DIGIT_1(TYPE_A),
	DIGIT_2(TYPE_A),
	DIGIT_3(TYPE_A),
	DIGIT_4(TYPE_A),
	DIGIT_5(TYPE_A),
	DIGIT_6(TYPE_A),
	DIGIT_7(TYPE_A),
	DIGIT_8(TYPE_A),
	DIGIT_9(TYPE_A),
};

const uint8_t digit_type_b[] = {
	DIGIT_0(TYPE_B),
	DIGIT_1(TYPE_B),
	DIGIT_2(TYPE_B),
	DIGIT_3(TYPE_B),
	DIGIT_4(TYPE_B),
	DIGIT_5(TYPE_B),
	DIGIT_6(TYPE_B),
	DIGIT_7(TYPE_B),
	DIGIT_8(TYPE_B),
	DIGIT_9(TYPE_B),
};

const uint8_t digit_type_c[] = {
	DIGIT_0(TYPE_C),
	DIGIT_1(TYPE_C),
	DIGIT_2(TYPE_C),
	DIGIT_3(TYPE_C),
	DIGIT_4(TYPE_C),
	DIGIT_5(TYPE_C),
	DIGIT_6(TYPE_C),
	DIGIT_7(TYPE_C),
	DIGIT_8(TYPE_C),
	DIGIT_9(TYPE_C),
};

const uint8_t digit_type_d[] = {
	DIGIT_0(TYPE_D),
	DIGIT_1(TYPE_D),
	DIGIT_2(TYPE_D),
	DIGIT_3(TYPE_D),
	DIGIT_4(TYPE_D),
	DIGIT_5(TYPE_D),
	DIGIT_6(TYPE_D),
	DIGIT_7(TYPE_D),
	DIGIT_8(TYPE_D),
	DIGIT_9(TYPE_D),
};

void lcd_init(struct lcd *const l) {
	lcd_clear_framebuffer(l);
	l->frame = 0;

#if LCD_TYPE==6
	gpio_set_direction_output(&PB_DDR, 7);
	gpio_set_direction_output(&PD_DDR, 0);
	gpio_set_direction_output(&PA_DDR, 4);
	l->display.port_w = &PA_ODR;
	l->display.port_cs = &PD_ODR;
	l->display.port_data = &PB_ODR;
	l->display.pin_w = 4;
	l->display.pin_cs = 0;
	l->display.pin_data = 7;
	PA_CR2 |= 1<<4;
	PD_CR2 |= 1<<0;
	PB_CR2 |= 1<<7;
	ht1621_init(&l->display);

	gpio_set_direction_output(&PB_DDR, 0);
	gpio_set_direction_output(&PB_DDR, 1);
	gpio_set_direction_output(&PB_DDR, 2);
	l->display_secondary.port_w = &PB_ODR;
	l->display_secondary.port_cs = &PB_ODR;
	l->display_secondary.port_data = &PB_ODR;
	l->display_secondary.pin_w = 1;
	l->display_secondary.pin_cs = 0;
	l->display_secondary.pin_data = 2;

	PB_CR2 |= 7;
	ht1621_init(&l->display_secondary);

	const uint8_t commands[] = {
		SYS_EN,
		RC256K,
		BIAS_THIRD_4_COM,
		LCD_ON
	};
	ht1621_send_commands(&l->display, commands, 4);
	ht1621_send_commands(&l->display_secondary, commands, 4);
#endif
}

void lcd_clear_framebuffer(struct lcd *const l) {
	memset(&l->framebuffer, 0x00, FRAMEBUFFER_SIZE);
}

void lcd_draw(struct lcd *const l) {
	++l->frame;

#if LCD_TYPE==6
	ht1621_write_buffer(&l->display, 0, l->framebuffer, 10);
	// Skip nibble and read from later in the frame buffer, lines up digits into single bytes
	ht1621_write_nibble(&l->display, 10, l->framebuffer[8]);
	ht1621_write_buffer(&l->display, 11, ((uint8_t*)&l->framebuffer)+5, 6);
	ht1621_write_nibble(&l->display, 17, l->framebuffer[8] << 4);
	ht1621_write_buffer(&l->display, 18, ((uint8_t*)&l->framebuffer)+9, 2);
	ht1621_write_nibble(&l->display, 20, l->framebuffer[12]);
	ht1621_write_buffer(&l->display, 21, ((uint8_t*)&l->framebuffer)+10, 4);
	ht1621_write_nibble(&l->display, 25, l->framebuffer[12] << 4);
	ht1621_write_buffer(&l->display, 26, ((uint8_t*)&l->framebuffer)+13, 6);

	ht1621_write_nibble(&l->display_secondary, 0, l->framebuffer[18]);
	ht1621_write_buffer(&l->display_secondary, 1, ((uint8_t*)&l->framebuffer)+16, 4);
	ht1621_write_nibble(&l->display_secondary, 5, l->framebuffer[18] << 4);
	ht1621_write_buffer(&l->display_secondary, 6, ((uint8_t*)&l->framebuffer)+19, 10);
#endif
}

void lcd_set_time(struct lcd *const l, const uint16_t minutes, const uint8_t seconds) {
#if LCD_TYPE==6

	l->framebuffer[0] = (l->framebuffer[0]&~digit_type_a[8]) | digit_type_a[(minutes/100)%10];
	l->framebuffer[1] = (l->framebuffer[1]&~digit_type_a[8]) | digit_type_a[(minutes/10)%10];
	l->framebuffer[2] = (l->framebuffer[2]&~digit_type_a[8]) | digit_type_a[minutes%10];

	l->framebuffer[3] = ~digit_type_a[8] | digit_type_a[seconds/10];
	l->framebuffer[4] = (l->framebuffer[4]&~digit_type_a[8]) | digit_type_a[seconds%10];
#endif
}

void lcd_set_battery(struct lcd *const l, const uint8_t level) {
#if LCD_TYPE==6
	// Clear
	l->framebuffer[12] &= ~0x08;
	l->framebuffer[4] &= digit_type_a[8];
	l->framebuffer[1] &= digit_type_a[8];
	l->framebuffer[2] &= digit_type_a[8];
	
	switch (level/2) {
		case 4:
			l->framebuffer[12] = 0x08 | (l->framebuffer[12] & ~0x08);
		case 3:
			l->framebuffer[4] = (l->framebuffer[4] & digit_type_a[8]) | ~digit_type_a[8];
		case 2:
			l->framebuffer[1] = (l->framebuffer[1] & digit_type_a[8]) | ~digit_type_a[8];
		case 1:
			l->framebuffer[2] = (l->framebuffer[2] & digit_type_a[8]) | ~digit_type_a[8];
		default:
		case 0:
			// Border
			l->framebuffer[0] = (l->framebuffer[0] & digit_type_a[8]) | ~digit_type_a[8];
			break;
	}
	if (l->frame%2) {
		switch(level) {
			case 8:
				l->framebuffer[12] &= ~0x08;
				break;
			case 6:
				l->framebuffer[4] &= digit_type_a[8];
				break;
			case 4:
				l->framebuffer[1] &= digit_type_a[8];
				break;
			case 2:
				l->framebuffer[2] &= digit_type_a[8];
				break;
			case 0:
				l->framebuffer[0] &= digit_type_a[8];
				break;
			default:
				break;
		}
	}
#endif
}

void lcd_set_speed_area(struct lcd *const l, const enum ControllerMode mode) {
#if LCD_TYPE==6
	// Clear
	l->framebuffer[5] = (l->framebuffer[5] & digit_type_a[8]);
	l->framebuffer[6] = (l->framebuffer[6] & digit_type_a[8]);
	l->framebuffer[8] = (l->framebuffer[8] & ~(1<<3));
	l->framebuffer[12] = (l->framebuffer[12] & ~(1<<1));

	switch (mode) {
		case Cruise:
			l->framebuffer[5] = (l->framebuffer[5] & digit_type_a[8]) | ~digit_type_a[8];
			break;
		case PAS:
			l->framebuffer[6] = (l->framebuffer[6] & digit_type_a[8]) | ~digit_type_a[8];
			break;
		case Throttle:
			l->framebuffer[8] = (l->framebuffer[8] & ~(1<<3)) | (1<<3);
			break;
		case Assist:
			l->framebuffer[12] = (l->framebuffer[12] & ~(1<<1)) | (1<<1);
			break;
		case Unknown:
		default:
			break;
	}
#endif
}

void lcd_set_backlight(struct lcd *const l, const bool enable) {
	gpio_write(l->port_backlight, l->pin_backlight, enable);
#if LCD_TYPE==6
	l->framebuffer[8] = (l->framebuffer[8] & ~(1<<7)) | ((1<<7)*enable);
#endif
}

void lcd_set_brake(struct lcd *const l, const bool applied) {
#if LCD_TYPE==6
	l->framebuffer[8] = (l->framebuffer[8] & ~(1<<6)) | ((1<<6)*applied);
#endif
}

void lcd_set_speed(struct lcd *const l, const uint16_t dkm, const bool average, const bool max) {
#if LCD_TYPE==6
	l->framebuffer[5] = (l->framebuffer[5] & ~digit_type_a[8]) | digit_type_a[(dkm/100)%10];
	l->framebuffer[6] = (l->framebuffer[6] & ~digit_type_a[8]) | digit_type_a[(dkm/10)%10];
	l->framebuffer[7] = ~digit_type_d[8] | digit_type_d[dkm%10];

	if (l->km) {
		l->framebuffer[8] = 1 | (l->framebuffer[8] & 0xfc);
	} else {
		l->framebuffer[8] = 2 | (l->framebuffer[8] & 0xfc);
	}

	l->framebuffer[12] = (l->framebuffer[12] & ~(1<<2)) | ((1<<2)*average);
	l->framebuffer[8] = (l->framebuffer[8] & ~(1<<2)) | ((1<<2)*max);
#endif
}

void lcd_set_pas(struct lcd *const l, const uint8_t pas) {
#if LCD_TYPE==6
	l->framebuffer[9] = digit_type_a[pas%10];
#endif
}

void lcd_set_cadence(struct lcd *const l, const uint8_t cadence) {
#if LCD_TYPE==6
	l->framebuffer[10] = (l->framebuffer[10]&~digit_type_a[8]) | digit_type_a[(cadence/10)%10];
	l->framebuffer[11] = (l->framebuffer[11]&~digit_type_a[8]) | digit_type_a[cadence%10];
	uint8_t icon = 0;
	if (cadence > 0) {
		switch (l->frame % 3) {
			case 0:
				icon = 0xa0;
				break;
			case 1:
				icon = 0xc0;
				break;
			case 2:
				icon = 0x90;
				break;
		}
	}
	l->framebuffer[12] = (l->framebuffer[12] & 0x0f) | icon;
#endif
}

void lcd_set_motor_area(struct lcd *const l, const uint16_t value, const bool motor, const bool temp, const bool watt) {
#if LCD_TYPE==6
	l->framebuffer[13] = digit_type_b[value%10];
	l->framebuffer[14] = digit_type_b[(value/10)%10];
	l->framebuffer[15] = digit_type_b[(value/100)%10] | (~digit_type_b[8] * (value>=1000));

	uint8_t v = 0;
	if (motor) {
		v = 0x80;
	}
	if (temp && l->celsius) {
		v|= 0x10;
	} else if (temp) {
		v|= 0x40;
	} else if (watt) {
		v|= 0x20;
	}
	l->framebuffer[18] = v | (l->framebuffer[18] & 0x0f);
#endif
}

void lcd_set_env_temp(struct lcd *const l, const int value) {
	int pvalue = abs(value);
#if LCD_TYPE==6

	l->framebuffer[16] = digit_type_b[pvalue%10] | (~digit_type_b[8] * (value<0));
	l->framebuffer[17] = digit_type_b[(pvalue/10)%10] | (~digit_type_b[8] * (pvalue >= 100));
	
	if (l->celsius) {
		l->framebuffer[18] = (l->framebuffer[18] & 0xf3) | 0x04;
	} else {
		l->framebuffer[18] = (l->framebuffer[18] & 0xf3) | 0x08;
	}
#endif
}

void lcd_set_odo_area(struct lcd *const l, const uint16_t value, const bool trip, const bool voltage) {
#if LCD_TYPE==6
	const bool odo = !trip && !voltage;
	const bool mi = !voltage && !l->km;
	const bool km = !voltage && l->km;
	l->framebuffer[19] = digit_type_c[value%10] | ~digit_type_c[8];
	l->framebuffer[20] = digit_type_b[(value/10)%10];
	l->framebuffer[21] = digit_type_b[(value/100)%10] | (~digit_type_b[8] * voltage);
	l->framebuffer[22] = digit_type_b[(value/1000)%10] | (~digit_type_b[8] * trip);
	l->framebuffer[23] = digit_type_b[(value/10000)%10] | (~digit_type_b[8] * odo);

	if (km) {
		l->framebuffer[18] = 0x01 | (l->framebuffer[18] & 0xfc);
	} else if (mi) {
		l->framebuffer[18] = 0x02 | (l->framebuffer[18] & 0xfc);
	} else {
		l->framebuffer[18] = l->framebuffer[18] & 0xfa;
	}
#endif
}

void lcd_set_status(struct lcd *const l, const struct State *const state) {
	lcd_set_brake(l, state->braking);
	lcd_set_backlight(l, state->lights);
	lcd_set_cadence(l, state->cadence);
	lcd_set_env_temp(l, state->env_temp);
	lcd_set_pas(l, state->pas_value);
	lcd_set_time(l, state->uptime/60, state->uptime%60);
	lcd_set_speed(l, state->speed, false, false);
	lcd_set_speed_area(l, state->mode);

	lcd_set_battery(l, state->battery_state);


	uint8_t m = (l->frame >> 4) & 0x03;

	switch (m) {
		case 0:
			lcd_set_odo_area(l, state->voltage, false, true);
			lcd_set_motor_area(l, state->controller_temp, true, true, false);
			break;
		case 1:
			lcd_set_odo_area(l, state->trip_odometer, true, false);
			lcd_set_motor_area(l, state->current*state->voltage/4, true, false, true);
			break;
		case 2:
		case 3:
			lcd_set_odo_area(l, state->total_odometer, true, false);
			lcd_set_motor_area(l, state->current*state->voltage/4, true, false, true);
			break;
	}
}
