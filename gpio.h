#pragma once

#include <stdint.h>
#include <stdbool.h>

volatile void gpio_set_direction_input(volatile uint8_t *const port, const uint8_t pin);

volatile void gpio_set_direction_output(volatile uint8_t *const port, const uint8_t pin);

static inline volatile void gpio_write(volatile uint8_t *const port, const uint8_t pin, const bool value);

static inline volatile bool gpio_read(volatile uint8_t *const port, const uint8_t pin);

static inline volatile void gpio_write(volatile uint8_t *const port, const uint8_t pin, const bool value) {
	// Warnings for this line are not a problem, its just saying it is optimising
	// out the alterative route when value is known at compile time
	if (value) {
		*port |= 1<<pin;
	} else {
		*port &= ~(1<<pin);
	}
}

static inline volatile bool gpio_read(volatile uint8_t *const port, const uint8_t pin) {
	return (((*port) & (1<<pin)) != 0);
}
